/**
 * java script file for shoots and ladders
 *
 * (c) 2014 Diwas Timilsina
*/


    //players 
    var player1 = 1;
    var player2 = 2;
    var currentPlayer= player1;
    var otherPlayer= player2;
var aheadPlayer = player1;
    
    //current score in the die
    var currentRoll=0;

    //pervious roll
    var prevRoll=0;
    
    // player 1's current Position
    var p1Score= 0;

    // player2's current Position
    var p2Score= 0;
    
    //boolean to decide whethe or not to start the game 
    var p1Start= false;
    var p2Start= false;


    //positions on the board
    var pos = [];
    
    //pos from the top
    var vert=[0,520,468,415,365,308,258,200,150,98,43];

    //pos from the left
    var horz=[0,645,695,750,800,855,905,960,1015,1065,1120];
    
//ladders and snakes
var upsDowns=[{id:4,dest:14},{id:9,dest:31},{id:17,dest:7},{id:20,dest:38},{id:28,dest:84},{id:40,dest:59},{id:51,dest:67},{id:64,dest:60},{id:63,dest:81},{id:89,dest:26},{id:95,dest:75},{id:99,dest:78}]

    //build the board
    this.populate();

// function to update the player position on the board
function updateBoard(){ 
    if (currentPlayer == player1){
	var elem = document.getElementById("green");
	if (p1Score == 0){
	    elem.style.top = "308px";
	    elem.style.left = "500px"
	}else{
	    elem.style.top = pos[p1Score].top+"px";
	    elem.style.left = pos[p1Score].left+"px";
	}
    } else if(currentPlayer == player2) {
	 var elem = document.getElementById("red");
	if(p2Score == 0){
	    elem.style.top = "365px";
	    elem.style.left = "500px"
	}else{	
	    elem.style.top = pos[p2Score].top+"px";
	    elem.style.left = pos[p2Score].left+"px";
 	}
    }
}



    // function to simulate rolling a die
    function rollDice(){
	var die = document.getElementById("die");
	var score = document.getElementById("score");
	prevRoll = currentRoll;
	currentRoll = Math.floor(Math.random() * 6) + 1;
	die.innerHTML = currentRoll;
	score.innerHTML = "Player "+currentPlayer+" rolled "+currentRoll+"!";
	var change= document.getElementById("player");
    

	//start the game only when current player gets two sixes in a 
	// row other wise switch to another player
	// also the counting starts only when a player starts the game
	if (currentPlayer == player1){
	    
	    if (!p1Start && currentRoll==6){
		if (currentRoll == prevRoll){
		    change.innerHTML="&nbsp;Player 1 Starts!";
		    p1Start = true;
		    p1Score = 1;
		}else{
		    change.innerHTML="&nbsp;Player 1 Goes Again!";
		}
	    }else{
		change.innerHTML= "&nbsp;Player 2 Goes Next!";
		if(p1Start){
		    updateScore();
		}
		currentPlayer = player2;
		otherPlayer = player1;
	    }
	}else{
	    if (!p2Start && currentRoll==6){
		
		if (currentRoll == prevRoll){
		    change.innerHTML="&nbsp;Player 2 Starts!";
		    p2Start = true;
		    p2Score = 1; 
		}else{
		    change.innerHTML="&nbsp;Player 2 Goes Again!";
		}
	    }else{
		change.innerHTML= "&nbsp;Player 1 Goes Next!";
		if(p2Start){
		    updateScore();
		}
		currentPlayer = player1;
		otherPlayer = player2;
	    }
	}
	updateBoard();
    }

//check if the game is won or not
function gameOver(){
    var announce = document.getElementById("player");
    if(p1Score==100){
	announce.innerHTML= "Game Over! Player "+player1+" won!";
	$("#rollButton").prop('disabled',true);
	return true;
    }else if (p2Score==100){
	announce.innerHTML= "Game Over! Player "+player2+" won!";
	$("#rollButton").prop('disabled',true);
	return true;
    }
    return false;
}

//function to update score
function updateScore(){
    var bool = gameOver();
    if(!bool){
	if (currentPlayer==player1){
	    var display = document.getElementById("player"); 
	    if((p1Score+currentRoll)<=100){
		p1Score+=currentRoll; 
		p1Score = check(p1Score);
		if (p1Score >=p2Score){
		    aheadPlayer = player1;
		}else{
		    aheadPlayer = player2;
		}
		collision();
		var p1pos = document.getElementById("p1Pos");
		p1pos.innerHTML= p1Score;
	    }else{
		display.innerHTML=" Cannot Complete the Move!";
		}
	}else{

	    var display = document.getElementById("player"); 
	    if((p2Score+currentRoll)<=100){
		p2Score+=currentRoll;
		p2Score = check(p2Score);
		if (p1Score >=p2Score){
		    aheadPlayer = player1;
		}else{
		    aheadPlayer = player2;
		}
		collision();
		var p2pos= document.getElementById("p2Pos");
		p2pos.innerHTML= p2Score;
	    }else{
		display.innerHTML=" Cannot Complete the Move!";
	    }
	}
    }
}


//check collision between players
function collision(){
    if(p1Score == p2Score && p1Score !=0){
	if(aheadPlayer==player1){
	    p1Score=0;
	    p1Start=false;
	    var p1pos= document.getElementById("p1Pos");
	    p1pos.innerHTML= p1Score;
	    
	}else if(aheadPlayer == player2){
	    p2Score=0;
	    p2Start=false;
	    var p2pos= document.getElementById("p2Pos");
	    p2pos.innerHTML= p2Score;
	}
    }
}

//check for snakes and ladders
function check(score){
    var ret=score;
    for(var i=0;i<upsDowns.length;i++){
	if(upsDowns[i].id == score){
	    updateBoard(); 
	    ret= upsDowns[i].dest;
	    break;
	}
    }
    return ret;
}

// restart the game
function restart(){
   
    p1Score = 0;
    p2Score = 0;
    p1Start = false;
    p2Start = false;
    currentRoll= 0;
   // updateBoard();
   // currentPlayer = player2;
    var announce = document.getElementById("player");
    var score = document.getElementById("score");
    announce.innerHTML="Let's Play Another Game!";
    score.innerHTML="Roll the Die!";
    $("#rollButton").prop('disabled',false);
    var p1pos= document.getElementById("p1Pos");
    var p2pos= document.getElementById("p2Pos");
    p1pos.innerHTML= p1Score;
    p2pos.innerHTML= p2Score;
    var elem = document.getElementById("red");
    var elem2 = document.getElementById("green");
    elem.style.top = "365px";
    elem.style.left = "500px"
    elem2.style.top = "308px";
    elem2.style.left = "500px"
}


// build board for the first time
function populate(){
    //keeps track of x direction
    var x=1;
    var right=true;
    var left=false;
    pos.push(null);

   //keeps track of y direction
    for (var y=1; y<=10;y++){
	if(right){
	    while(x<=10){
		pos.push({top:vert[y],left:horz[x]});
		if(x%10 == 0){
		    var bool = right;
		    right = left;
		    left = bool; 
		}
		x++;
	    }
	}else{
	    while(x>1){
		x--;
		pos.push({top:vert[y],left:horz[x]});
		if(x%10 == 1){
		    var bool = right;
		    right = left;
		    left = bool; 
		}
	    }
	}
    }
    
} 
